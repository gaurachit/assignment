sub init()
  m.port = CreateObject("roMessagePort")
  m.top.functionName = "processRequest"

  m.request = CreateObject("roUrlTransfer")
  m.request.SetCertificatesFile("common:/certs/ca-bundle.crt")
  m.apiToken = m.global.apiToken
  m.request.AddHeader("Content-Type" , "application/json")
  m.request.InitClientCertificates()
  m.request.setMessagePort(m.port)    
end sub

function processRequest()
  ' ? "URI: ",m.top.uri
  ' ? "PARAMS: ",m.top.params
  m.request.setUrl(m.top.uri)
  if m.apiToken <> invalid
    m.request.AddHeader("X-API-TOKEN" , m.apiToken)
  end if
  if m.top.action = "GET"
    getOperation()
  else if m.top.action = "POST"
    postOperation()
  end if
end function



function getOperation()
  res = m.request.GetToString()
  m.top.content = {
    "success": true
    "data" : parseJSON(res)
  }
end function

function postOperation()
  if(m.request.AsyncPostFromString(m.top.params))
    while true
      msg = wait(0, m.port)
      if type(msg) = "roUrlEvent"
        code = msg.getResponseCode()
        if code = 200
          m.top.content = {
            "success": true
            "data" : parseJSON(msg.Getstring())
          }
        else
          m.top.content = {
            "success": false
            "data" : invalid
          }
        end if
        exit while
      end if
    end while
  end if
 '? "**********************************************************************"
end function

