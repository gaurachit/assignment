sub init()
    m.top.observeField("visible", "onVisibleChanged")
    m.assetMarkupGrid = m.top.findNode("assetMarkupGrid")
    m.assetMarkupGrid.observeField("itemSelected", "assetSelected")
    m.config = getConfig()
end sub
  
sub onVisibleChanged()
    if m.top.visible
      if m.assetItems = invalid
        getAssetList()
      else 
        m.assetMarkupGrid.setFocus(true)
        m.global.removeField("allVod")
        m.global.addFields({"allVod":m.assetItems})
      end if
    end if
end sub


sub getAssetList()
  m.allVODTask = CreateObject("roSGNode", "RequestHandler")
  m.allVODTask.action = "GET"
  m.allVODTask.uri = m.config.allVODURI
  m.allVODTask.observeField("content", "allVODTaskCompleted")
  m.allVODTask.control = "RUN"
end sub

sub allVODTaskCompleted()
  result = m.allVODTask.content
  if result.success
    m.assetItems = result.data
    m.global.removeField("allVod")
    m.global.addFields({"allVod":m.assetItems})
    drawAssets()
    if m.global.deeplink <> invalid
      checkAssetAvailable()
    end if
  end if
end sub

sub checkAssetAvailable()
  assetFound = false
  for index = 0 to m.assetItems.count() - 1
    if m.global.deeplink.contentId  =  m.assetItems[index].id AND m.global.deeplink.mediaType  =  m.assetItems[index].type
      assetFound = true
      m.global.removeField("selectedAssetIndex")
      m.global.addFields({"selectedAssetIndex": index})
    end if
  end for
  if assetFound
    m.top.showDetailComponent = true
    m.top.visible = false
  else
    m.global.removeField("deeplink")
  end if
end sub

sub drawAssets()
  content = createObject("roSGNode", "ContentNode")
  for each item in m.assetItems
    listItem = content.createChild("CustomContentNode")
    if item.poster =  invalid
      item.poster = "pkg:/images/poster.png"
    end if
    listItem.hdgridposterurl = item.poster
    listItem.title = item.title
    listItem.year = item.year
  end for
  m.assetMarkupGrid.content = content
  m.assetMarkupGrid.setFocus(true)
end sub

sub  assetSelected()
  selectedIndex = m.assetMarkupGrid.itemSelected
  m.global.removeField("selectedAssetIndex")
  m.global.addFields({"selectedAssetIndex": selectedIndex})
  m.top.showDetailComponent = true
  m.top.visible = false
end sub

function onKeyEvent(key as String, press as Boolean) as Boolean
  handler = false
  if press then
      if key = "options"
        m.top.showSearchComponent = true
        handler = true
    end if
  end if
  return handler
end function
  