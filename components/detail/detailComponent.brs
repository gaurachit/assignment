sub init()
    m.top.observeField("visible", "onVisibleChanged")
    m.assetPoster = m.top.findNode("assetPoster")
    m.lblAsset = m.top.findNode("lblAsset")
    m.lblYear = m.top.findNode("lblYear")
    m.lblDescription = m.top.findNode("lblDescription")
    m.btnPlay = m.top.findNode("btnPlay")
    m.btnPlay.observeField("buttonSelected", "onBtnPlaySelected")
    m.assetVideoPlayer = m.top.findNode("assetVideoPlayer")
    m.assetVideoPlayer.observeField("position", "onPositionChanged")
    m.config = getConfig()
end sub
  
sub onVisibleChanged()
  if m.top.visible
    m.selectedAssetIndex = m.global.selectedAssetIndex
    m.allVod = m.global.allVod
    drawDetailScreen()
  end if
end sub

sub drawDetailScreen()
  m.selectedAsset = m.allVod[m.selectedAssetIndex]
  if m.selectedAsset.poster =  invalid
    m.selectedAsset.poster = "pkg:/images/poster.png"
  end if
  m.assetPoster.uri = m.selectedAsset.poster
  m.lblAsset.text = m.selectedAsset.title
  m.lblYear.text = m.selectedAsset.year
  m.lblDescription.text = m.selectedAsset.description
  if m.selectedAsset.type = "movie"
    m.btnPlay.visible= true
    m.btnPlay.setFocus(true)
    if m.global.deeplink <> invalid
      getVideoStream()
    end if  
  else
    m.btnPlay.visible= false
    m.top.setFocus(true)
  end if
  if m.global.deeplink <> invalid
    m.global.removeField("deeplink")
  end if
end sub

sub onBtnPlaySelected()
  getVideoStream()
end sub

sub getVideoStream()
  m.vodStreamTask = CreateObject("roSGNode", "RequestHandler")
  m.vodStreamTask.action = "GET"
  m.vodStreamTask.uri = m.config.vodStreamURI+"/"+m.selectedAsset.id+"/stream?drm=true"
  m.vodStreamTask.observeField("content", "vodStreamTaskCompleted")
  m.vodStreamTask.control = "RUN"
end sub

sub vodStreamTaskCompleted()
  result = m.vodStreamTask.content
  ' ? FormatJSON(result, 0)
  if result.success
    m.currentStream = result.data
    if m.currentStream <> invalid
      PlayVideo()
    end if
  end if
end sub

sub PlayVideo()
  savedPlaybackPosition = getPlaybackPosition((m.selectedAsset.id).toStr())
  contentNode = CreateObject("roSGNode", "ContentNode")
  contentNode.title = m.selectedAsset.title
  contentNode.streamFormat = m.currentStream.type
  contentNode.url = m.currentStream.url
  contentNode.drmParams = {
    keySystem: m.currentStream.drmType
    licenseServerURL: m.currentStream.drmUrl
  }
  m.assetVideoPlayer.content = contentNode
  if savedPlaybackPosition <> invalid
    m.assetVideoPlayer.seek = StrToI(savedPlaybackPosition)
  end if
  m.assetVideoPlayer.control = "play"
  m.assetVideoPlayer.visible = true
  m.assetVideoPlayer.setFocus(true)
end sub 

sub onPositionChanged()
  videoPlaybackPosition = m.assetVideoPlayer.position
  savePlaybackPosition((m.selectedAsset.id).toStr(), (videoPlaybackPosition).toStr())
  params ={
    id: m.selectedAsset.id, 
    progress: videoPlaybackPosition
  }
  m.vodHeartbeatTask = CreateObject("roSGNode", "RequestHandler")
  m.vodHeartbeatTask.action = "POST"
  m.vodHeartbeatTask.uri = m.config.vodHeartbeatURI
  m.vodHeartbeatTask.params = formatJSON(params, 0)
  m.vodHeartbeatTask.observeField("content", "vodHeartbeatTaskCompleted")
  m.vodHeartbeatTask.control = "RUN"
end sub

sub vodHeartbeatTaskCompleted()
  result = m.vodHeartbeatTask.content
  ' ? FormatJSON(result, 0)
end sub

function onKeyEvent(key as String, press as Boolean) as Boolean
  handler = false
  if press then
      if key = "back"
        if m.assetVideoPlayer.visible
          m.assetVideoPlayer.control = "stop"
          m.assetVideoPlayer.visible = false
          m.btnPlay.setFocus(true)
        else
          m.top.showGridComponent = true
          m.top.visible = false
        end if
        handler = true
      else if key = "left"
        if m.selectedAssetIndex - 1 >= 0
          m.selectedAssetIndex = m.selectedAssetIndex - 1
        else
          m.selectedAssetIndex = m.allVod.count() - 1
        end if
        drawDetailScreen()
        handler = true
      else if key = "right"
        if m.selectedAssetIndex + 1 < m.allVod.count()
          m.selectedAssetIndex = m.selectedAssetIndex + 1
        else
          m.selectedAssetIndex = 0
        end if
        drawDetailScreen()
        handler = true
    end if
  end if
  return handler
end function
  