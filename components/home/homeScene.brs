sub init()
  m.gridComponent = m.top.findnode("gridComponent")
  m.searchComponent = m.top.findnode("searchComponent")
  m.detailComponent = m.top.findnode("detailComponent")
  m.config = getConfig()
  getAccessToken()
end sub

sub getAccessToken()
  m.apiTokenTask = CreateObject("roSGNode", "RequestHandler")
  m.apiTokenTask.action = "GET"
  m.apiTokenTask.uri = m.config.apiTokenURI
  m.apiTokenTask.observeField("content", "apiTokenTaskCompleted")
  m.apiTokenTask.control = "RUN"
end sub

sub apiTokenTaskCompleted()
  result = m.apiTokenTask.content
  if result.success
    m.global.addFields({"apiToken": result.data.apiToken})
    m.gridComponent.visible = true
  end if
end sub

sub showSearchComponentChange()
  if m.top.showSearchComponent = true
    m.gridComponent.visible = false
    m.searchComponent.visible = true
    m.top.showSearchComponent = false
  end if
end sub

sub showGridComponentChange()
  if m.top.showGridComponent = true
    m.searchComponent.visible = false
    m.gridComponent.visible = true
    m.top.showGridComponent = false
  else if m.top.showGridComponentFromDetail = true
    m.detailComponent.visible = false
    m.gridComponent.visible = true
    m.top.showGridComponentFromDetail = false
  end if
end sub


sub showDetailComponentChange()
  if m.top.showDetailComponent = true
    m.gridComponent.visible = false
    m.detailComponent.visible = true
    m.top.showDetailComponent = false
  else if m.top.showDetailComponentFromSearch = true
    m.searchComponent.visible = false
    m.detailComponent.visible = true
    m.top.showDetailComponentFromSearch = false
  end if
end sub