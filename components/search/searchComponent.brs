sub init()
    m.top.observeField("visible", "onVisibleChanged")
    m.txtSearch = m.top.findNode("txtSearch")
    m.lblMessage = m.top.findNode("lblMessage")
    m.btnSearch = m.top.findNode("btnSearch")
    m.btnSearch.observeField("buttonSelected", "onSearchButtonSelected")
    m.radioFilter = m.top.findNode("radioFilter")
    m.itemList = m.top.findNode("itemList")
    m.itemList.observeField("rowItemSelected", "assetRowItemSelected")
    m.filterType = ["","Movie", "Series"]
    m.config = getConfig()
    m.filterBy = ""
    m.searchTerm = ""
end sub
  
sub onVisibleChanged()
  if m.top.visible
    m.sceneNode = getSceneNode()
    m.lblMessage.text = ""
    m.txtSearch.text = ""
    m.radioFilter.checkedItem = 0
    m.itemList.content = invalid
    m.txtSearch.active = true
    m.top.setFocus(true)
  end if
end sub

sub showKeyboard()
  m.keyboard = CreateObject("roSGNode","KeyboardDialog")
  m.keyboard.title = "Enter search term"
  m.keyboard.optionsDialog = false
  m.keyboard.keyboard.textEditBox.secureMode = false
  m.keyboard.buttons = ["Ok", "Cancel"]
  m.keyboard.observeField("buttonSelected", "keyboardButtonCallback")
  m.sceneNode.dialog = m.keyboard
end sub

sub keyboardButtonCallback()
    selectedIndex = m.keyboard.buttonSelected
    enterdText = m.keyboard.text
    m.keyboard.close = true
    if selectedIndex = 0
        if enterdText <> invalid and enterdText <> ""
          m.txtSearch.text = enterdText
          m.txtSearch.active = false
          m.radioFilter.setFocus(true)
        end if
    end if
end sub

sub onSearchButtonSelected()
  m.searchTerm = m.txtSearch.text
  if m.radioFilter.checkedItem = 0
    m.filterBy = ""
  else 
    m.filterBy = "&filter="+m.filterType[m.radioFilter.checkedItem]
  end if
  getAssetListBySearchTerm()
end sub

sub getAssetListBySearchTerm()
  m.vodSearchTask = CreateObject("roSGNode", "RequestHandler")
  m.vodSearchTask.action = "GET"
  m.vodSearchTask.uri = m.config.vodSearchURI + m.searchTerm + m.filterBy
  m.vodSearchTask.observeField("content", "vodSearchTaskCompleted")
  m.vodSearchTask.control = "RUN"
end sub

sub vodSearchTaskCompleted()
  result = m.vodSearchTask.content
  if result.success AND result.data <> invalid
    m.assetItems = result.data
    if m.assetItems.count() > 0
      m.lblMessage.text = ""
      m.global.removeField("allVod")
      m.global.addFields({"allVod":m.assetItems})
      drawAssets()
    else
      m.itemList.content = invalid
      m.lblMessage.text = "No Result found."
    end if
  end if
end sub

sub drawAssets()
  content = createObject("roSGNode", "ContentNode")
  sectionContent =  content.createChild("ContentNode")
  for each item in m.assetItems
    if item.poster =  invalid
      item.poster = "pkg:/images/poster.png"
    end if
    listItem = sectionContent.createChild("CustomContentNode")
    listItem.hdgridposterurl = item.poster
    listItem.title = item.title
    listItem.year = item.year
  end for
  m.itemList.content = content
  m.itemList.setFocus(true)
end sub

sub assetRowItemSelected()
  selectedIndex = m.itemList.rowItemSelected[1]
  m.global.removeField("selectedAssetIndex")
  m.global.addFields({"selectedAssetIndex":selectedIndex})
  m.top.showDetailComponent = true
end sub

function onKeyEvent(key as String, press as Boolean) as Boolean
  handler = false
  if press then
    if key = "back"
      m.top.showGridComponent = true
      m.top.setFocus(false)
      handler = true
    else if key = "OK"
      if m.txtSearch.active
        showKeyboard()
      end if
      handler = true
    else if key = "down"
      if m.txtSearch.active
        m.txtSearch.active = false
        m.radioFilter.setFocus(true)
      else if m.radioFilter.hasFocus()
        m.btnSearch.setFocus(true)
      end if
      handler = true
    else if key = "up"
      if m.btnSearch.hasFocus()
        m.radioFilter.setFocus(true)
      else if m.radioFilter.hasFocus()
        m.radioFilter.setFocus(false)
        m.top.setFocus(true)
        m.txtSearch.active = true
      else if m.itemList.hasFocus()
          m.btnSearch.setFocus(true)
      end if
      handler = true
    end if
  end if
  return handler
end function