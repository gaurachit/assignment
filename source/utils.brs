function getConfig()
	baseURL = "http://192.168.0.109:8080"
	return  {
		"regSection"		: "assignment"
		"apiTokenURI"		: baseURL + "/"
		"allVODURI"			: baseURL + "/vod"
		"vodSearchURI"		: baseURL + "/vod/search?s="		
		"vodStreamURI"		: baseURL + "/vod/"
		"vodHeartbeatURI"	: baseURL + "/vod/heartbeat"
	}
end function

function getSceneNode()
	scene = m.top
	while scene.getParent() <> invalid
	  scene = scene.getParent()
	end while
	return scene
end function

sub savePlaybackPosition(key, value)
    sec = CreateObject("roRegistrySection",  getConfig().regSection)
    sec.Write(key, value)
end sub

function getPlaybackPosition(key)
    sec = CreateObject("roRegistrySection", getConfig().regSection)
	if sec.Exists(key)
		return sec.Read(key)
	end if
	return invalid	
end function