sub Main(args as dynamic)
    screen = CreateObject("roSGScreen")
    m.port = CreateObject("roMessagePort")
    screen.setMessagePort(m.port)

    m.global = screen.getGlobalNode()
    checkAndAplyDeepLinking(args)
    scene = screen.CreateScene("HomeScene")
    scene.signalBeacon("AppLaunchComplete")
    screen.show()
    input = CreateObject("roInput")
    input.SetMessagePort(m.port)
    while(true)
        msg = wait(0, m.port)
        msgType = type(msg)
        if msgType = "roSGScreenEvent"
            if msg.isScreenClosed()
                return
            end if
        else if msgType = "roInputEvent"
            if msg.isInput() = true
                inputMsgData = msg.GetInfo()
                checkAndAplyDeepLinking(inputMsgData)
            end if
        end if
    end while
end sub


function checkAndAplyDeepLinking(args)
    if args <> invalid AND args.contentId <> invalid AND args.mediaType <> invalid
        m.global.removeField("deeplink")
        m.global.addFields({"deeplink" : args})
    end if
end function